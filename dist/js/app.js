(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var x = 0, y = 0,
    vx = 0, vy = 0,
  ax = 0, ay = 0;

var sphere = document.getElementById("sphere");

if (window.DeviceMotionEvent != undefined) {
  window.ondevicemotion = function(e) {
    ax = event.accelerationIncludingGravity.x * 5;
    ay = event.accelerationIncludingGravity.y * 5;
    document.getElementById("accelerationX").innerHTML = e.accelerationIncludingGravity.x;
    document.getElementById("accelerationY").innerHTML = e.accelerationIncludingGravity.y;
    document.getElementById("accelerationZ").innerHTML = e.accelerationIncludingGravity.z;

    if ( e.rotationRate ) {
      document.getElementById("rotationAlpha").innerHTML = e.rotationRate.alpha;
      document.getElementById("rotationBeta").innerHTML = e.rotationRate.beta;
      document.getElementById("rotationGamma").innerHTML = e.rotationRate.gamma;
    }
  }

  setInterval( function() {
    var landscapeOrientation = window.innerWidth/window.innerHeight > 1;
    if ( landscapeOrientation) {
      vx = vx + ay;
      vy = vy + ax;
    } else {
      vy = vy - ay;
      vx = vx + ax;
    }
    vx = vx * 0.98;
    vy = vy * 0.98;
    y = parseInt(y + vy / 50);
    x = parseInt(x + vx / 50);

    boundingBoxCheck();

    sphere.style.top = y + "px";
    sphere.style.left = x + "px";

  }, 25);
}


function boundingBoxCheck(){
  if (x<0) { x = 0; vx = -vx; }
  if (y<0) { y = 0; vy = -vy; }
  if (x>document.documentElement.clientWidth-20) { x = document.documentElement.clientWidth-20; vx = -vx; }
  if (y>document.documentElement.clientHeight-20) { y = document.documentElement.clientHeight-20; vy = -vy; }

}
},{}]},{},[1])